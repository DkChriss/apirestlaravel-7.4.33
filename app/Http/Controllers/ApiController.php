<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Data;

class ApiController extends Controller
{
    public function getData(Request $request) {

        $newdata = new Data();
        $newdata->properties = $request->all();
        $newdata->method = $request->method();
        $newdata->save();
        return response()->json('Se registro correctamente los datos', 200);
    }

    public function postData(Request $request) {
        $data = new Data();
        $data->properties = $request->all();
        $data->method = $request->method();
        $data->save();
        return response()->json('Se registro correctamente los datos', 200);
    }

    public function patchData(Request $request) {
        $data = new Data();
        $data->properties = $request->all();
        $data->method = $request->method();
        $data->save();
        return response()->json('Se actualizo correctamente los datos', 200);
    }

    public function putData(Request $request) {
        $data = new Data();
        $data->properties = $request->all();
        $data->method = $request->method();
        $data->save();
        return response()->json('Se actualizo correctamente los datos', 200);
    }

    public function deleteData(Request $request) {
        $data = new Data();
        $data->properties = $request->all();
        $data->method = $request->method();
        $data->save();
        return response()->json('Se elimmino correctamente la informacion', 200);
    }

    public function login(Request $request) {

    }
}
