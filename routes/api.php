<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/get_data', [ApiController::class, 'getData']);
Route::post('/post_data',[ApiController::class, 'postData']);
Route::patch('/patch_data',[ApiController::class, 'patchData']);
Route::put('/put_data',[ApiController::class, 'putData']);
Route::delete('/delete_data', [ApiController::class, 'deleteData']);
